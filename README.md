UniRxShowcase
===

Assets in use
---

UniRx - Created by Yoshifumi Kawai  
GitHub: https://github.com/neuecc/UniRx  
License: https://gitlab.com/syousuke1104/unirxshowcase/-/blob/master/Assets/Plugins/UniRx/LICENSE

UniTask  - Created by Yoshifumi Kawai / Cysharp, Inc.  
GitHub: https://github.com/Cysharp/UniTask  
License: https://gitlab.com/syousuke1104/unirxshowcase/-/blob/master/Assets/Plugins/UniTask/LICENSE

FancyScrollView - Created by setchi  
GitHub: https://github.com/setchi/FancyScrollView  
License: https://github.com/setchi/FancyScrollView/blob/master/LICENSE  

M+ Fonts - Created by M+ FONTS PROJECT  
URL: https://mplus-fonts.osdn.jp/about2.html  
License: https://gitlab.com/syousuke1104/unirxshowcase/-/blob/master/Assets/MPlusFonts/LICENSE_J  
