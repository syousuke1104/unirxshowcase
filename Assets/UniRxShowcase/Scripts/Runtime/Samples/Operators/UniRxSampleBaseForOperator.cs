﻿namespace UniRxShowcase.Samples.Operators {

	public abstract class UniRxSampleBaseForOperator : UniRxSampleBase {

		protected override string DirectoryPath => "Assets/UniRxShowcase/Scripts/Runtime/Samples/Operators";

	}
}
