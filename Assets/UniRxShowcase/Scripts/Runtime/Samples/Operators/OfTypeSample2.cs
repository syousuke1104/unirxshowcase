﻿using System;
using System.Linq;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class OfTypeSample2 : UniRxSampleBaseForOperator {

		// キャスト可能なメッセージのみ通過させる
		// IObservable<TResult> OfType<TSource, TResult>(this IObservable<TSource> source)

		// 型指定する代わりにwitnessプロパティに値を設定
		// float型の2のみ通過させる
		public override IObservable<Unit> RunAsync() =>
			new object[] { 1, 2f, '3', "four", 5d, 6 }
				.ToObservable()
				.OfType(0f)
				.Debug()
				.AsUnitObservable();
	}
}
