﻿using System;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class DistinctUntilChangedSample1 : UniRxSampleBaseForOperator {

		// 直前のメッセージと比較して重複を排除する
		// IObservable<T> DistinctUntilChanged<T>(this IObservable<T> source)
		// IObservable<T> DistinctUntilChanged<T>(this IObservable<T> source, IEqualityComparer<T> comparer)
		// IObservable<T> DistinctUntilChanged<T, TKey>(this IObservable<T> source, Func<T, TKey> keySelector)
		// IObservable<T> DistinctUntilChanged<T, TKey>(this IObservable<T> source, Func<T, TKey> keySelector, IEqualityComparer<TKey> comparer)

		// 1, 2, 1, 3 を通過させる
		public override IObservable<Unit> RunAsync() =>
			new[] { 1, 2, 2, 1, 3, 3 }
				.ToObservable()
				.DistinctUntilChanged()
				.Debug()
				.AsUnitObservable();
	}
}
