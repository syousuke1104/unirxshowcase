﻿using System;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class IgnoreElementsSample1 : UniRxSampleBaseForOperator {

		// OnNextメッセージを無視する
		// IObservable<T> IgnoreElements<T>(this IObservable<T> source)

		public override IObservable<Unit> RunAsync() =>
			Observable.Range(0, 10)
				.IgnoreElements()
				.Debug()
				.AsUnitObservable();
	}
}
