﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class DistinctSample2 : UniRxSampleBaseForOperator {

		// 過去に入力されたメッセージを除外する
		// ストリーム内でメッセージが重複しなくなる
		// IObservable<TSource> Distinct<TSource>(this IObservable<TSource> source, IEqualityComparer<TSource> comparer)

		// IEqualityComparer<TSource>を指定するタイプ
		// 重複を排除した Hello, World を通過させる
		public override IObservable<Unit> RunAsync() =>
			new[] { "Hello", "World", "hello", "world" }
				.ToObservable()
				.Distinct(new LowerStringsComparer())
				.Debug()
				.AsUnitObservable();


		// Equals, GetHashCode の比較で true を返すようにする必要がある
		private class LowerStringsComparer : IEqualityComparer<string> {

			public bool Equals(string x, string y) {
				return x.ToLower() == y.ToLower();
			}

			public int GetHashCode(string obj) {
				return obj.ToLower().GetHashCode();
			}
		}
	}
}
