﻿using System;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class OfTypeSample1 : UniRxSampleBaseForOperator {

		// キャスト可能なメッセージのみ通過させる
		// IObservable<TResult> OfType<TSource, TResult>(this IObservable<TSource> source)

		// int型の1と6のみ通過させる
		public override IObservable<Unit> RunAsync() =>
			new object[] { 1, 2f, '3', "four", 5d, 6 }
				.ToObservable()
				.OfType<object, int>()
				.Debug()
				.AsUnitObservable();
	}
}
