﻿using System;
using System.Linq;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class WhereSample2 : UniRxSampleBaseForOperator {

		// 条件を満たすメッセージのみ通過させるオペレータ
		// IObservable<T> Where<T>(this IObservable<T> source, Func<T, int, bool> predicate)

		// 5以上の偶数のみ通過させる
		public override IObservable<Unit> RunAsync() =>
			Observable.Range(0, 10)
				.Where((x, count) => x % 2 == 0 && count >= 5)
				.Debug()
				.AsUnitObservable();
	}
}
