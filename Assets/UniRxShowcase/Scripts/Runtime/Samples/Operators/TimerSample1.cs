﻿using System;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class TimerSample1 : UniRxSampleBaseForOperator {

		// 一定時間後にメッセージを発行するファクトリメソッド
		// IObservable<long> Timer(TimeSpan dueTime)

		// 5秒後にメッセージを発行する
		public override IObservable<Unit> RunAsync() =>
			Observable.Timer(TimeSpan.FromSeconds(5))
				.Debug()
				.AsUnitObservable();
	}
}
