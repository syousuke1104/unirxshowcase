﻿using System;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class TimerSample2 : UniRxSampleBaseForOperator {

		// 一定時間後にメッセージを発行するファクトリメソッド
		// IObservable<long> Timer(DateTimeOffset dueTime)
		// DateTimeOffsetでもタイマーを設定可能

		// 実行した時刻から3秒後にメッセージを発行する
		public override IObservable<Unit> RunAsync() =>
			Observable.Timer(DateTimeOffset.Now + TimeSpan.FromSeconds(3))
				.Debug()
				.AsUnitObservable();
	}
}
