﻿using System;
using System.Linq;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class WhereSample1 : UniRxSampleBaseForOperator {

		// 条件を満たすメッセージのみ通過させるオペレータ
		// IObservable<T> Where<T>(this IObservable<T> source, Func<T, bool> predicate)

		// 値が5未満のメッセージを通過させる
		public override IObservable<Unit> RunAsync() =>
			Observable.Range(0, 10)
				.Where(x => x < 5)
				.Debug()
				.AsUnitObservable();
	}
}
