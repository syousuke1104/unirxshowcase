﻿using System;
using System.Linq;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class DistinctSample3 : UniRxSampleBaseForOperator {

		// 過去に入力されたメッセージを除外する
		// ストリーム内でメッセージが重複しなくなる
		// IObservable<TSource> Distinct<TSource, TKey>(this IObservable<TSource> source, Func<TSource, TKey> keySelector)

		// プロパティやメソッドの返り値を比較するタイプ
		// 文字列の長さが同じメッセージは排除する(Helloのみ通過)
		public override IObservable<Unit> RunAsync() =>
			new[] { "Hello", "World", "hello", "world" }
				.ToObservable()
				.Distinct(x => x.Length)
				.Debug()
				.AsUnitObservable();
	}
}
