﻿using System;
using System.Linq;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class DistinctSample1 : UniRxSampleBaseForOperator {

		// 過去に入力されたメッセージを除外する
		// ストリーム内でメッセージが重複しなくなる
		// IObservable<TSource> Distinct<TSource>(this IObservable<TSource> source)

		// 重複を排除した 1, 2, 5 を通過させる
		public override IObservable<Unit> RunAsync() =>
			new[] { 1, 2, 2, 1, 5, 2 }
				.ToObservable()
				.Distinct()
				.Debug()
				.AsUnitObservable();
	}
}
