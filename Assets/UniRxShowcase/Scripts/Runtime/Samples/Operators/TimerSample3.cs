﻿using System;
using UniRx;
using UniRx.Diagnostics;

namespace UniRxShowcase.Samples.Operators {

	public class TimerSample3 : UniRxSampleBaseForOperator {

		// 一定時間後にメッセージを発行するファクトリメソッド
		// IObservable<long> Timer(TimeSpan dueTime, TimeSpan period)

		// 3秒待機後、1秒間隔でメッセージを発行する
		public override IObservable<Unit> RunAsync() =>
			Observable.Timer(TimeSpan.FromSeconds(3), TimeSpan.FromSeconds(1))
				.Debug()
				.AsUnitObservable();
	}
}
