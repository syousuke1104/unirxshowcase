﻿using System;
using System.IO;
using UniRx;

namespace UniRxShowcase.Samples {

	public abstract class UniRxSampleBase {

		public string Label { get; set; }

		protected abstract string DirectoryPath { get; }

		private string m_sampleCode;

		public string GetSampleCode() {
			return m_sampleCode ??= File.ReadAllText(Path.Combine(DirectoryPath, GetType().Name + ".cs"));
		}

		public abstract IObservable<Unit> RunAsync();

	}
}
