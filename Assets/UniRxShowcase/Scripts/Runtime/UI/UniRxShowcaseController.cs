﻿using Cysharp.Threading.Tasks;
using System.Linq;
using TMPro;
using UniRx;
using UniRxShowcase.Samples;
using UniRxShowcase.Samples.Operators;
using UnityEngine;
using UnityEngine.UI;

namespace UniRxShowcase.UI {

	public class UniRxShowcaseController : MonoBehaviour {

		[SerializeField]
		private UniRxSampleFSRect m_uniRxSampleFSRect = default;

		[SerializeField]
		private TextMeshProUGUI m_sampleCodeText = default;

		[SerializeField]
		private Button m_cancelButton = default;

		[SerializeField]
		private Button m_runButton = default;

		private void Start() {

			var uniRxSamples = new UniRxSampleBase[] {
				new WhereSample1() { Label = "Where" },
				new WhereSample2() { Label = "Where - with message count"},
				new OfTypeSample1() { Label = "OfType" },
				new OfTypeSample2() { Label = "OfType - with witness" },
				new IgnoreElementsSample1() { Label = "IgnoreElements" },
				new DistinctSample1() { Label = "Distinct" },
				new DistinctSample2() { Label = "Distinct - with EqualityComparer" },
				new DistinctSample3() { Label = "Distinct - with key selector" },
				new DistinctUntilChangedSample1() { Label = "DistinctUntilChanged" },
				new TimerSample1() { Label = "Timer" },
				new TimerSample2() { Label = "Timer - with DateTimeOffset"},
				new TimerSample3() { Label = "Timer - with Interval" }
			}.Select(s => new UniRxSampleFSRectItem.ItemData(s))
			.ToList();

			m_uniRxSampleFSRect.UpdateItems(uniRxSamples);

			m_uniRxSampleFSRect.OnClickItemAsObservable
				.Select(item => item.UniRxSample.GetSampleCode())
				.Subscribe(s => m_sampleCodeText.text = s);

			var runningRP = new BoolReactiveProperty();

			runningRP.AddTo(this);
			runningRP.Select(x => !x).SubscribeToInteractable(m_runButton);
			runningRP.SubscribeToInteractable(m_cancelButton);

			m_runButton.OnClickAsObservable()
				.WithLatestFrom(m_uniRxSampleFSRect.OnClickItemAsObservable, (_, item) => item.UniRxSample)
				.Subscribe(sample => UniTask.Void(async state => {
					var (sample, runningRP) = state;

					runningRP.Value = true;

					Debug.Log(sample.Label);

					await sample.RunAsync()
						.TakeUntil(m_cancelButton.OnClickAsObservable().Take(1))
						.DefaultIfEmpty();

					runningRP.Value = false;
				}, (sample, runningRP)));
		}
	}
}
