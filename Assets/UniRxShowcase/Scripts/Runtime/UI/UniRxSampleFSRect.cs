﻿using FancyScrollView;
using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace UniRxShowcase.UI {

	public class UniRxSampleFSRect : FancyScrollRect<UniRxSampleFSRectItem.ItemData, UniRxSampleFSRect.ViewContext> {

		[SerializeField]
		private GameObject m_cellPrefab = default;

		private float? m_cellSize;

		protected override float CellSize => m_cellSize ??= m_cellPrefab.GetComponent<RectTransform>().rect.height;

		protected override GameObject CellPrefab => m_cellPrefab;

		public void UpdateItems(IList<UniRxSampleFSRectItem.ItemData> items) {
			UpdateContents(items);
		}

		public IObservable<UniRxSampleFSRectItem.ItemData> OnClickItemAsObservable => Context.OnClickSubject.AsObservable();

		public class ViewContext : FancyScrollRectContext {

			public readonly Subject<UniRxSampleFSRectItem.ItemData> OnClickSubject = new Subject<UniRxSampleFSRectItem.ItemData>();

		}
	}
}
