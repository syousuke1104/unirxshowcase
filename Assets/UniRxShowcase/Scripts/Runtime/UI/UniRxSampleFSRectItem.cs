﻿using FancyScrollView;
using System.Linq;
using TMPro;
using UniRx;
using UniRxShowcase.Samples;
using UnityEngine.UI;

namespace UniRxShowcase.UI {

	public class UniRxSampleFSRectItem : FancyScrollRectCell<UniRxSampleFSRectItem.ItemData, UniRxSampleFSRect.ViewContext> {

		private ItemData? m_itemData;

		private TextMeshProUGUI m_labelText;

		public override void Initialize() {
			base.Initialize();
			m_labelText = transform.GetComponentInChildren<TextMeshProUGUI>();

			transform.GetComponent<Button>()
				.OnClickAsObservable()
				.Where(_ => m_itemData.HasValue)
				.Select(_ => m_itemData.Value)
				.Subscribe(Context.OnClickSubject);
		}

		public override void UpdateContent(ItemData itemData) {
			m_itemData = itemData;
			m_labelText.SetText(itemData.UniRxSample.Label);
		}

		public struct ItemData {

			public UniRxSampleBase UniRxSample { get; }

			public ItemData(UniRxSampleBase uniRxSample) {
				UniRxSample = uniRxSample;
			}
		}
	}
}
